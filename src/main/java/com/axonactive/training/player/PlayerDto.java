/**
 * 
 */
package com.axonactive.training.player;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author nksthuong
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public @Data class PlayerDto implements Serializable{
	
	private String name;
	
	private String socialInsuranceNumber;
	
	private int number;
	
	private Date dateOfBirth;
	
	private Gender gender;
	
	private Integer teamId;

}
