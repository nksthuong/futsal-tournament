package com.axonactive.training.player;

import java.util.Objects;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.axonactive.training.team.Team;
import com.axonactive.training.team.TeamService;

import com.axonactive.training.tournament.TournamentMessageKeys;
import com.axonactive.training.tournament.TournamentMessageService;

@Stateless
public class PlayerService {
	@PersistenceContext
	EntityManager em ;
	
	@EJB
	private TeamService teamService;
	
	@EJB 
	TournamentMessageService tournamentMessageService;
	
	public Integer addPlayer(Player player) {
		this.em.persist(player);
		
		return player.getId();
	}
	
	
	public Player findPlayerById(Integer playerId){
		Player player = em.find(Player.class,playerId);
		
		if(Objects.isNull(player)) {
			throw new IllegalArgumentException(tournamentMessageService.get(TournamentMessageKeys.PLAYER_DOES_NOT_EXIST));
		}
		return player;
		
	}
	
	public Player convertToPlayer(PlayerDto playerDto) {
		Player player = new Player(playerDto.getName(),
								   playerDto.getSocialInsuranceNumber(),
								   playerDto.getNumber(),
								   playerDto.getDateOfBirth(),
								   playerDto.getGender()); 
		
		Team team = this.teamService.findTeamByID(playerDto.getTeamId());
		
		player.setTeam(team);
		
		return player;
	}
	
	
	public PlayerDto convertToPlayerDto(Player player) {
		PlayerDto playerDto = new PlayerDto(player.getName(), player.getSocialInsuranceNumber(), player.getNumber(), player.getDateOfBirth(), player.getGender(), player.getTeam().getId());
		return playerDto;
		
	}
}
