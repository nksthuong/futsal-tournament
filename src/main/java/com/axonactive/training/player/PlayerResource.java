/**
 * 
 */
package com.axonactive.training.player;


import java.net.URI;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.axonactive.training.player.Gender;
import com.axonactive.training.player.Player;
import com.axonactive.training.player.PlayerDto;
import com.axonactive.training.tournament.TournamentMessageKeys;
import com.axonactive.training.tournament.TournamentMessageService;


/**
 * @author nksthuong
 *
 */

@Stateless
@Path("players")
public class PlayerResource {
	@EJB
	PlayerService playerService;
	
	@EJB
	TournamentMessageService tournamentMessageService;
	
	@PersistenceContext
	EntityManager em;
	
	@Context
	UriInfo uriInfo;
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addPlayer(PlayerDto playerDto) {
		Player player = this.playerService.convertToPlayer(playerDto);
		this.playerService.addPlayer(player);
		URI location = this.uriInfo.getAbsolutePathBuilder().path(player.getId().toString()).build();
		return Response.created(location).build();
	}
	
	@GET
	@Path("{id}")
	@Produces({MediaType.APPLICATION_JSON})
	public Response getPlayerById(@PathParam("id") Integer playerId) {
		
		try {
			Player player = this.playerService.findPlayerById(playerId);
			PlayerDto playerDto = this.playerService.convertToPlayerDto(player);
			return Response.ok(playerDto).build();
		} catch (Exception e) {
			// TODO: handle exception
			throw new IllegalArgumentException(tournamentMessageService.get(TournamentMessageKeys.PLAYER_DOES_NOT_EXIST));
		}
		
//		Player player = this.playerService.findPlayerById(playerId);
//		PlayerDto playerDto = this.playerService.convertToPlayerDto(player);
	
//		return Response.ok(playerDto).build();
		
	}
	@GET
	public JsonArray buildJsonPlayer() {
		JsonArrayBuilder playerList = Json.createArrayBuilder();
		List<Player> playersFromDB = em.createQuery("Select p From Player p").getResultList();
		for (Player player : playersFromDB) {
			playerList.add(this.createJsonPlayer(player.getName(), player.getSocialInsuranceNumber(), player.getNumber(), player.getDateOfBirth(),	 player.getGender()));
		}
		return playerList.build();
	}
	
	
	JsonObject createJsonPlayer(String name, String socialnumber, int number, Date dob, Gender gender ) {
		return Json.createObjectBuilder().
				add("name", name).
				add("socialnumber", socialnumber).
				add("number", number).
				add("dob", dob.toString()).
				add("gender", gender.toString()).build();
	}
	
}
