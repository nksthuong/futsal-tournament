package com.axonactive.training.player;

/**
 * @author nksthuong
 *
 */
import javax.persistence.AttributeConverter;

public class GenderConverter implements AttributeConverter<Gender, Integer> {

	@Override
	public Integer convertToDatabaseColumn(Gender attribute) {
		// TODO Auto-generated method stub
		return attribute.getValue();
	}

	@Override
	public Gender convertToEntityAttribute(Integer dbData) {
		switch (dbData) {
		
		case -1: 
			return Gender.UNKNOW;
			
		case 0:
			return Gender.FEMALE;
			
		case 1:
			return Gender.MALE;
		
		default: 
			throw new IllegalStateException("Unknow gender");
		}
	}

}
