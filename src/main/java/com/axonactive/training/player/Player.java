/**
 * The player class instance the information of the player of Futsal game 
 */
package com.axonactive.training.player;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TemporalType;

import com.axonactive.training.team.Team;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author htdat Last updated oct-10-2018
 */
@Entity
@Table(name = "tbl_player")
@AllArgsConstructor @NoArgsConstructor
public @Data class Player implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	/**
	 * To store the name of player
	 */
	@Column(name = "name", length = 100, nullable = false, unique = false)
	private String name;
	
	@Column(name = "social_insurance_number", length = 10, unique = true, nullable = false)
	private String socialInsuranceNumber;
	
	@Column(name = "number", nullable = false)
	private int number;
	
	@Column(name = "date_of_birth")
	@javax.persistence.Temporal(TemporalType.DATE)
	private Date dateOfBirth;
	
	@Column(name = "gender", nullable = false)	
	@Enumerated()
	private Gender gender;
	
	@ManyToOne
	private Team team;

	public Player(String name, String socialInsuranceNumber, int number, Date dateOfBirth, Gender gender) {
		this.name = name;
		this.socialInsuranceNumber = socialInsuranceNumber;
		this.number = number;
		this.dateOfBirth = dateOfBirth;
		this.gender = gender;
	}

	
}
