package com.axonactive.training.player;


/**
 * @author nksthuong
 *
 */
import lombok.Getter;

public enum Gender {
	MALE(0),
	FEMALE(1),
	UNKNOW(2);
	
	private @Getter int value;
	Gender(int value) {
		this.value = value;
	}
	
}
