/**
 * 
 */
package com.axonactive.training.team;

/**
 * @author nksthuong
 *
 */

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import javax.persistence.Table;

import com.axonactive.training.player.Player;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author htdat Last updated oct-10-2018
 */

@AllArgsConstructor
@NoArgsConstructor
@Entity
@NamedQueries({
	@NamedQuery(name = Team.GET_PLAYER_LIST, query = "Select p from Team t JOIN t.players p where t.id = 5"),
	@NamedQuery(name = Team.GET_TEAM, query = "SELECT t FROM Team t WHERE t.name = :name")
})

@Table(name="tbl_team")
public @Data class Team {

	public static final String PREFIX = "com.axonactive.training.tournament.TeamService";
	public static final String GET_PLAYER_LIST = PREFIX + ".GetPlayerList";
	public static final String GET_TEAM = PREFIX + ".GetTeam";
	
	
//	@PersistenceContext
//	private EntityManager em;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "name", nullable = false, unique = true, length = 50)
	@JoinColumn(name = "team_id")
	private String name;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "team_id")
	private List<Player> players;
	
	public static final int MAX_MEMBER_OF_TEAM = 12;
	public static final int MIN_MEMBER_OF_TEAM = 7;
	
	public Team(String name) {
		this.name = name;
	}

}
