package com.axonactive.training.team;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Stateless;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.*;

import com.axonactive.training.player.PlayerService;

/**
 * @author nksthuong
 *
 */
@Stateless
public class TeamService {



	@PersistenceContext
	EntityManager em;	
	
	 @EJB
	 PlayerService playerService;
	
	public Team findTeamByID(Integer teamID) {
		return this.em.find(Team.class, teamID);
	}
	
	public Team findTeamByName(String thisTeamName) {
		Team team = new Team();
		team = this.em.createNamedQuery(Team.GET_TEAM, Team.class).setParameter("name", thisTeamName).getSingleResult();
		return team;
	}
	public void addTeam(Team team) {
		this.em.persist(team);
	}
	
	public Team convertToTeam(TeamDto teamDto) {
		Team team = new Team(teamDto.getName());
//		Team team = findTeamByName(teamDto.getName());
		return team;
	}
	
	public TeamDto convertToTeamDto(Team team) {
		
		TeamDto  teamDto = new TeamDto(team.getName());
		
		return teamDto;
	}
	
//	@PostConstruct
//	public void addthisTeam() {
//		Calendar dob_Player = new GregorianCalendar(); 
//		
//		for(int i = 0; i < 5; i++) {
//			Player player = new Player();
//			player.setName("Pham Ngoc Hai" + i);
//			player.setSocialInsuranceNumber("110011001" + i);
//			player.setDateOfBirth(dob_Player.getTime());
//			player.setNumber(10 + i);
//			player.setGender(Gender.MALE);
//			List<Player> players = new ArrayList<>();
//			players.add(player);
//			team = new Team();
//			team.setName("Hai " + i);
//			team.setPlayers(players);
//			em.persist(team);
//		}
//		addTeam(team);
//		updateTeam(team);
////		
////		getPlayerList();
////		getTeam();
//	}
//	
//	public void getPlayerList() {
//		List<Player> players_test = em.createNamedQuery(Team.GET_PLAYER_LIST).getResultList();
//		Logger log = Logger.getLogger(PlayerService.class.getName());
//		log.info(players_test.toString());
//	}
//	public void getTeam() {
//		Team thisTeam = (Team) em.createNamedQuery(Team.GET_TEAM).getSingleResult();
//		
//		Logger log = Logger.getLogger(PlayerService.class.getName());
//		log.info(thisTeam.toString());
//		
//		em.remove(thisTeam);
//		
//		List<Team> thisTeam1 = em.createNamedQuery(Team.GET_TEAM).getResultList();
//		log.info(thisTeam1.toString());
//	
//		updateTeam(thisTeam);
//	}
//	
//	public void updateTeam(Team team) {
//		team.setName("ConnAction");
//		em.merge(team);
//	}
}
