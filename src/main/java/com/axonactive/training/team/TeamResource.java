/**
 * 
 */
package com.axonactive.training.team;

import java.net.URI;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 * @author nksthuong
 *
 */

@Stateless
@Path("teams")

public class TeamResource {
	
	@EJB
	TeamService teamService;
	
	@Context
	UriInfo uriInfo;
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	public Response addTeam(TeamDto teamDto) {
		
		Team team = this.teamService.convertToTeam(teamDto);
		this.teamService.addTeam(team);
		URI location = this.uriInfo.getAbsolutePathBuilder().path(team.getId().toString()).build();
		return Response.created(location).build();
		
	}
	
	@GET
	@Path("{id}")
	@Produces({MediaType.APPLICATION_JSON})
	public Response getTeamById(@PathParam("id") Integer teamId) {
		
		Team team = this.teamService.findTeamByID(teamId);
		TeamDto teamDto = this.teamService.convertToTeamDto(team);
		
		return Response.ok(teamDto).build();
	}
}
