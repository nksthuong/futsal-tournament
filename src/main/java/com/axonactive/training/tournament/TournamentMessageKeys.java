/**
 * 
 */
package com.axonactive.training.tournament;

import com.axonactive.common.resource.ResourceKey;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author nksthuong
 *
 */
@AllArgsConstructor
public enum TournamentMessageKeys implements ResourceKey {
	
	PLAYER_DOES_NOT_EXIST("PLayer_Does_Not_Exist");
	
	private String key;

	@Override
	public String getName() {

		return this.key;
	}
	
	
	
}
