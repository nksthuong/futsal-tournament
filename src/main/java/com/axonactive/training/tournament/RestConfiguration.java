/**
 * 
 */
package com.axonactive.training.tournament;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author nksthuong
 *
 */
@ApplicationPath("api")
public class RestConfiguration extends Application {

	
}
