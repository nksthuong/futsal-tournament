/**
 * 
 */
package com.axonactive.training.tournament;

import java.io.Serializable;

import javax.ejb.Stateless;

import com.axonactive.common.resource.i18n.PropertiesMessageService;



/**
 * @author nksthuong
 *
 */
@Stateless
public class TournamentMessageService extends PropertiesMessageService implements Serializable{

	public TournamentMessageService() {
		super("ValidationMessages");
	}

}
