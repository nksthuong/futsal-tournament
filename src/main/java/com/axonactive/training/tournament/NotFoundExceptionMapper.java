package com.axonactive.training.tournament;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


@Provider
public class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {

	 public Response toResponse(NotFoundException exception) {
	  return Response.status(Status.NOT_FOUND).entity("Oops! Resource could not be found.").build();
	 } 
}