/**
 * 
 */
package com.axonactive.training.tournament;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author nksthuong
 *
 */
@Provider
public class ExceptionHandler implements ExceptionMapper<Throwable>{

	@Override
	public Response toResponse(Throwable exception) {
		
		return Response.status(Status.BAD_REQUEST).entity("Error:" + exception.getMessage()).build();
	}
	
}
